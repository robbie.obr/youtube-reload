(function(){

	var AIRHORN = new Audio('resources/AIRHORN.mp3');
	var VINYL_REWIND = new Audio('resources/VINYL_REWIND.mp3');
	var RELOAD_TIME_OFFSET = 0.00;
	var CURRENT_VOLUME = 100;

	var callContentScript = function(oPayload, fnResponseCallback) {
		chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
			chrome.tabs.sendMessage(tabs[0].id, oPayload, fnResponseCallback);
		});
	};	

	var playVinylRewind = function(fnThen) {
		VINYL_REWIND.pause();
		VINYL_REWIND.onended = fnThen;
		VINYL_REWIND.currentTime = 0;
		VINYL_REWIND.volume = CURRENT_VOLUME/100;
		VINYL_REWIND.play();
	};

	var playAirhorn = function(fnThen) {
		AIRHORN.pause();
		AIRHORN.onended = fnThen;
		AIRHORN.currentTime = 0;
		AIRHORN.volume = CURRENT_VOLUME/100;
		AIRHORN.play();
	};

	var stopAll = function() {
		VINYL_REWIND.pause();
		VINYL_REWIND.currentTime = 0;
		AIRHORN.pause();
		AIRHORN.currentTime = 0;
		callContentScript({
			actions: ['PAUSE']
		});
	};

	var getTimeOffset = function(inputValue) {
		var time = 0;		
		time = Math.floor(inputValue) * 60;
		time += (inputValue % 1) * 100;
		return time;
	}

	var fullReload = function() {
		playVinylRewind(function(){
			playAirhorn(function(){
				callContentScript({
					actions: ['RESTART', 'PLAY'],
					start_time: RELOAD_TIME_OFFSET
				});
			});
		});
	};

	$('#youtube-reload-full-reload-button').click(function(event){
		stopAll();
		fullReload();
	});


	$('#youtube-reload-airhorn-button').click(function(event){
		stopAll();
		playAirhorn();
	});

	$('#youtube-reload-vinyl-rewind-button').click(function(event){
		stopAll();
		playVinylRewind();
	});

	$('#youtube-reload-start-offset').change(function(event){
		if(Number.isNaN(event.target.value*1)) {
			$(event.target.parentElement).addClass('has-error');
		} else {
			$(event.target.parentElement).removeClass('has-error');
			RELOAD_TIME_OFFSET = getTimeOffset(event.target.value*1);
		}
		console.log(event);
	});

	$('#youtube-reload-advnced-reload-button>button').click(function(event){
		stopAll();
		callContentScript({
			actions: ['FULL'],
			start_time: RELOAD_TIME_OFFSET
		});
	});

	$('#h-slider').slider({
	    range: "min",
	    min: 0,
	    max: 100,
	    value: CURRENT_VOLUME,
	    slide: function (event, ui) {
	        CURRENT_VOLUME = ui.value;
	        $("#amount").val(CURRENT_VOLUME);
	        AIRHORN.volume = ui.value/100;
	        VINYL_REWIND.volume = ui.value/100;
	        $('#youtube-reload-volume-text-value').val(CURRENT_VOLUME);
	    }
	});

	$('#youtube-reload-volume-text-value').val(CURRENT_VOLUME);
	
}());