(function() {

	chrome.runtime.onMessage.addListener(
	    function(request, sender, sendResponse) {
	    	var video = $('video')[0];
	    	request.actions.forEach(function(action) {
		    	switch(action){
		    		case 'FULL':
				    	video.pause();
				    	video.currentTime = request.start_time;
				    	video.play();
				    	break;
				     case 'PAUSE':
				     	video.pause();
				     	break;
				     case 'RESTART':
				     	video.currentTime = request.start_time ? request.start_time : 0;
				     	break;
				     case 'PLAY':
				     	video.play();
				     	break;
		    	}
	    	});
	    	sendResponse({success: true});
	    }
	);
}());